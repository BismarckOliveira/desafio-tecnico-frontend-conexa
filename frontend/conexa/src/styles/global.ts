import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`

html, body, #root, #root>div {
  margin: 0px;

}

h1, h2 , h3 {
  font-family: 'Montserrat';
  margin: 0;
}

span, strong, p{
  font-family: 'Nunito';
  font-style: normal;
  font-weight: normal;
}

button{
  cursor: pointer;
  :focus {
    outline: none;
  }
}


`;
