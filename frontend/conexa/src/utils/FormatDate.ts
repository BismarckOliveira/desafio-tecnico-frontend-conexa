interface IDate {
  date: string;
}

const FormatDate = ({ date }: IDate): string => {
  const localDate = new Date(date);

  return `${localDate.toLocaleString()}`;
};

export { FormatDate };
