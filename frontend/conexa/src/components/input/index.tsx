import React, { InputHTMLAttributes, useEffect, useRef } from 'react';
import { useField } from '@unform/core';
import { FiAlertCircle } from 'react-icons/fi';
import { AiFillEyeInvisible } from 'react-icons/ai';
import { Container, Error, Content } from './style';

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  password?: 'password';
}

const Input: React.FC<InputProps> = ({ name, children, password, ...rest }) => {
  const inputRef = useRef(null);
  const { fieldName, registerField, error } = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
    });
  }, [fieldName, registerField]);

  return (
    <Container isErrored={!!error}>
      <Content>
        <input ref={inputRef} {...rest} />
        {password && !error && <AiFillEyeInvisible />}
        {error && (
          <Error title={error}>
            <FiAlertCircle />
          </Error>
        )}
        {children}
      </Content>

      <hr />
    </Container>
  );
};

export default Input;
