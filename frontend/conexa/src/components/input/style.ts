import styled, { css } from 'styled-components';
import ToolTip from '../tooltip';

interface IContainerProps {
  isErrored: boolean;
}
export const Container = styled.div<IContainerProps>`
  input {
    flex: 1;
    border: 0;
    margin-top: 5px;
    width: 211px;
    box-sizing: border-box;

    ::placeholder {
      color: #999392;
      font-style: italic;
    }
    :focus {
      outline: 0;
    }
  }

  svg {
    border: none;
    background: none;
    font-size: 16px;
    color: #999392;
  }

  hr {
    border: 1px solid #dad2d0;
  }

  ${props =>
    props.isErrored &&
    css`
      svg {
        color: #c35050;
      }
    `}
`;

export const Content = styled.div`
  display: flex;
`;

export const Error = styled(ToolTip)`
  display: flex;
  align-items: center;
  padding-right: 5px;
  span {
    display: flex;

    justify-content: center;
  }
`;
