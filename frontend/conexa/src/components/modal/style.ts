import styled from 'styled-components';
import { shade } from 'polished';

export const ModalContainer = styled.div`
  width: 100%;
  height: 100%;
  z-index: 10;
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 0;
  left: 0;
  background: ${shade(0.8, '#1111')};
`;

export const Container = styled.div`
  width: 70%;
  height: 70%;
  background: #ffff;
  border-radius: 20px;
  display: flex;
  flex-direction: column;
  padding: 12px;
`;

export const Actions = styled.div`
  display: flex;
  direction: rtl;

  button {
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 12px;
    width: 65px;
    height: 32px;
  }

  button + button {
    margin-right: 8px;
    background: #ffff;
    border: 2px solid #2e50d4;
    color: #2e50d4;

    &:hover {
      background: ${shade(0.1, '#ffffff')};
    }
  }

  @media screen and (max-width: 800px) {
    direction: none;
    justify-content: center;
  }
`;
export const Content = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;
