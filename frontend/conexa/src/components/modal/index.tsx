import React, { useCallback } from 'react';
import { ModalContainer, Container, Content, Actions } from './style';
import Button from '../button';

interface IModalProps {
  onclose(): void;
  onSchedule(): void;
}

const Modal: React.FC<IModalProps> = ({ onclose, onSchedule, children }) => {
  const AgendarCunsulta = useCallback(() => {
    onSchedule();
    onclose();
  }, [onclose, onSchedule]);

  return (
    <ModalContainer>
      <Container>
        <Content>{children}</Content>
        <Actions>
          <Button onClick={AgendarCunsulta}>Agendar</Button>
          <Button onClick={onclose}>Fechar</Button>
        </Actions>
      </Container>
    </ModalContainer>
  );
};

export default Modal;
