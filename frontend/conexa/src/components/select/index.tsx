import React, { SelectHTMLAttributes } from 'react';
import { Select } from './styled';

type ISelectProps = SelectHTMLAttributes<HTMLSelectElement>;

const SelectComponent: React.FC<ISelectProps> = ({ children, ...rest }) => {
  return (
    <Select name="pacienteList" id="paciente" {...rest}>
      {children}
    </Select>
  );
};

export default SelectComponent;
