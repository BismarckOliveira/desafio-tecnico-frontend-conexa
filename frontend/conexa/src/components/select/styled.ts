import styled from 'styled-components';

export const Select = styled.select`
  height: 40px;
  width: 235px;
  border-radius: 5px;
  border: none;
  background: #ffff;
  color: #2e50d4;
  font-size: 16px;
`;
