import styled, { css } from 'styled-components';

interface IToastProps {
  type?: 'success' | 'error' | 'info';
}

const ToatTypesVariations = {
  info: css`
    background: #2e50d4;
    border: 1px solid #2e50d4;
    color: #2e50d4;
  `,
  success: css`
    background: #e6fffa;
    border: 1px solid #e6fffa;
    color: #2e656a;
  `,
  error: css`
    background: #fddede;
    border: 1px solid #fddede;
    color: #c53030;
  `,
};

export const Container = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  padding: 30px;
  overflow: hidden;
`;

export const Toast = styled.div<IToastProps>`
  width: 360px;
  position: relative;
  padding: 16px 30px 16px 16px;
  border-radius: 10px;
  box-shadow: 2px 2px 8px rgba(0, 0, 0, 0.2);
  display: flex;

  & + div {
    margin-top: 8px;
  }

  border: 1px solid #2e50d4;
  color: #2e50d4;

  ${props => ToatTypesVariations[props.type || 'info']}

  > svg {
    margin: 4px 12px 0px 0px;
  }

  div {
    flex: 1;

    p {
      margin-top: 4px;
      font-size: 14px;
      opacity: 0.8;
      line-height: 20px;
    }
  }
  button {
    position: absolute;
    right: 16px;
    top: 19px;
    opacity: 0.6;
    border: 0;
    background: transparent;
    color: inherit;
  }
`;
