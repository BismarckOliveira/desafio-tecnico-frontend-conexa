import React from 'react';
import { Container } from './style';

interface IToolTipProps {
  title: string;
  className?: string;
}

const ToolTip: React.FC<IToolTipProps> = ({ title, className, children }) => {
  return (
    <Container className={className}>
      {children}
      <span>{title}</span>
    </Container>
  );
};

export default ToolTip;
