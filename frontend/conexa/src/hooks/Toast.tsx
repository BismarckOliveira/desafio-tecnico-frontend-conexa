import React, { createContext, useCallback, useContext, useState } from 'react';
import { uuid } from 'uuidv4';
import ToastContainer from '../components/toastContainer';

export interface IToastMessage {
  id: string;
  type: 'info' | 'success' | 'error';
  title: string;
  description: string;
}

interface IToastContextDate {
  addToast(messages: Omit<IToastMessage, 'id'>): void;
  removeToast(id: string): void;
}

const ToastContext = createContext<IToastContextDate>({} as IToastContextDate);

const ToastProvider: React.FC = ({ children }) => {
  const [messages, setMessages] = useState<IToastMessage[]>([]);

  const addToast = useCallback(
    ({ title, description, type }: Omit<IToastMessage, 'id'>) => {
      const id = uuid();
      const THREE_SECONDS = 3000;
      const toast = {
        id,
        type,
        title,
        description,
      };

      setTimeout(() => {
        removeToast(id);
      }, THREE_SECONDS);

      setMessages([...messages, toast]);
    },
    [messages],
  );
  const removeToast = useCallback((id: string) => {
    setMessages(state => state.filter(message => message.id !== id));
  }, []);

  return (
    <ToastContext.Provider value={{ addToast, removeToast }}>
      {children}
      <ToastContainer messages={messages} />
    </ToastContext.Provider>
  );
};

function useToast(): IToastContextDate {
  const context = useContext(ToastContext);

  if (!context) {
    throw new Error('useToast must be used within a ToastProvider');
  }

  return context;
}

export { ToastProvider, useToast };
