import React, { useCallback, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { BiHelpCircle } from 'react-icons/bi';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import * as Yup from 'yup';
import {
  Container,
  NavBar,
  NavBarContent,
  Section,
  BoxIlustration,
  Content,
  ContentContainer,
} from './style';
import Input from '../../components/input';
import ImgIlustratio from '../../assets/ilustration.png';
import Button from '../../components/button';
import logo from '../../assets/logo-conexa.png';
import { useAuth, ISinginCredentials } from '../../hooks/Auth';
import { useToast } from '../../hooks/Toast';
import getValidationErrors from '../../utils/getValidationErrors';

const SingIn: React.FC = () => {
  const { signIn } = useAuth();
  const { addToast } = useToast();
  const history = useHistory();

  const formRef = useRef<FormHandles>(null);

  const HandleSubmit = useCallback(
    async (data: ISinginCredentials) => {
      try {
        formRef.current?.setErrors({});

        const schema = Yup.object().shape({
          email: Yup.string()
            .required('E-mail obrigatório')
            .email('Digite um email valido'),
          password: Yup.string().min(6, 'No mínimo 6 digitos'),
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        await signIn({
          email: data.email,
          password: data.password,
        });

        history.push('/dashboard');
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);

          formRef.current?.setErrors(errors);
        }

        addToast({
          type: 'error',
          title: 'Aconteceu um erro',
          description: 'Suas credenciais estão incorretas ',
        });
      }
    },
    [signIn, history, addToast],
  );

  return (
    <Container>
      <NavBar>
        <NavBarContent>
          <img src={logo} alt="ConexaLogo" />
        </NavBarContent>
      </NavBar>
      <Section>
        <BoxIlustration>
          <h1>Faça Login</h1>
          <img src={ImgIlustratio} alt="ilustração" />
        </BoxIlustration>
        <ContentContainer>
          <Content>
            <Form ref={formRef} onSubmit={HandleSubmit}>
              <div>
                <span>E-mail</span>
                <Input name="email" placeholder="Digite seu e-mail" />
              </div>
              <div>
                <span>
                  Senha
                  <BiHelpCircle />
                </span>
                <Input
                  name="password"
                  type="password"
                  placeholder="Digite sua senha"
                  password="password"
                />
              </div>
              <Button type="submit">Entrar</Button>
            </Form>
          </Content>
        </ContentContainer>
      </Section>
    </Container>
  );
};

export default SingIn;
