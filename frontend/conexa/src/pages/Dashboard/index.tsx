import React, { useEffect, useCallback, useState, ChangeEvent } from 'react';
import { TextField } from '@material-ui/core';
import dayjs from 'dayjs';
import {
  Container,
  NavBar,
  NavBarContent,
  Header,
  Section,
  Description,
  Footer,
  Agenda,
  Title,
} from './style';
import logo from '../../assets/logo-conexa.png';
import Button from '../../components/button';
import Modal from '../../components/modal';
import { useAuth } from '../../hooks/Auth';
import api from '../../services/api';
import { FormatDate } from '../../utils/FormatDate';
import Select from '../../components/select';
import { useToast } from '../../hooks/Toast';

interface IPacient {
  id: number;
  first_name: string;
  last_name: string;
}

interface Iconsultations {
  id: number;
  patientId: number;
  date: string;
}

const Dasboard: React.FC = () => {
  const { signOut, name } = useAuth();
  const [consults, setconsult] = useState<Iconsultations[]>([]);
  const [pacients, setpacients] = useState<IPacient[]>([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [patientSelected, setpatientSelected] = useState('');
  const [schedule, setSchedule] = useState({
    date: dayjs().locale('pt-br').format('YYYY-MM-DD'),
    hour: dayjs().locale('pt-br').format('HH:mm'),
  });

  const { addToast } = useToast();

  const HandleSingOut = useCallback(() => {
    signOut();
  }, [signOut]);

  const reqListConsultations = useCallback(async () => {
    const response = await api.get(`consultations?_expand=${name}`);

    setconsult(response.data);
  }, []);

  useEffect(() => {
    reqListConsultations();
  }, [name]);

  useEffect(() => {
    async function ListPacients() {
      const listPacients = await api.get(`patients`);
      setpacients(listPacients.data);
    }
    ListPacients();
  }, []);

  const getPacientFullName = (id: number) => {
    const pacientInfo = pacients.find(pacient => pacient.id === id);

    return `${pacientInfo?.first_name} ${pacientInfo?.last_name}`;
  };

  const MarK = () => {
    setIsModalVisible(true);
  };

  const setScheduleDate = (eventDate: ChangeEvent<HTMLInputElement>) => {
    const { value } = eventDate.target;

    setSchedule({
      ...schedule,
      date: value,
    });
  };
  const setScheduleHour = (eventHour: ChangeEvent<HTMLInputElement>) => {
    const { value } = eventHour.target;

    setSchedule({
      ...schedule,
      hour: value,
    });
  };

  const HandleCreateConsult = useCallback(async () => {
    const date = new Date(`${schedule.date} ${schedule.hour}`);
    const dateCompare = dayjs().isBefore(date);
    const patientId = parseInt(patientSelected, 16);
    let error = false;

    if (!dateCompare) {
      addToast({
        type: 'error',
        title: 'Data Invalida',
        description: 'A data Selecionada não esta disponivel',
      });
      error = true;
    }
    if (!patientSelected) {
      addToast({
        type: 'error',
        title: 'Paciente Inválido',
        description: 'Paciente não selecionado',
      });
      error = true;
    }
    if (error) return;

    api
      .post('consultations', {
        patientId,
        date,
      })
      .then(() => {
        addToast({
          type: 'success',
          title: 'Consulta Marcada',
          description: 'Sua consulta foi marcada',
        });
        reqListConsultations();
      })
      .catch(() => {
        addToast({
          type: 'error',
          title: 'Paciente Inválido',
          description: 'operação invalida',
        });
      });
  }, [patientSelected, schedule, addToast]);

  return (
    <Container>
      <NavBar>
        <NavBarContent>
          <img src={logo} alt="ConexaLogo" />
          <div>
            <span>Olá Dr {name}</span>
            <Button onClick={HandleSingOut}>Sair</Button>
          </div>
        </NavBarContent>
      </NavBar>
      <Header>
        <h1>Consultas</h1>
      </Header>
      <Section>
        <ul>
          <p>{consults.length} consultas agendadas</p>
          {consults.map(consult => (
            <li key={consult.id}>
              <Description>
                <strong>{getPacientFullName(consult.patientId)}</strong>
                <span>{FormatDate({ date: consult.date })}</span>
              </Description>
              <Button>Atender</Button>
            </li>
          ))}
        </ul>
      </Section>
      <Footer>
        <hr />
        <div>
          <Button className="ajuda">Ajuda</Button>
          <Button className="novaConsulta" onClick={MarK}>
            Agendar Consulta
          </Button>
        </div>
      </Footer>
      {isModalVisible && (
        <Modal
          onSchedule={HandleCreateConsult}
          onclose={() => setIsModalVisible(false)}
        >
          <Agenda>
            <Title>Agendamento</Title>
            <Select
              onChange={e => {
                setpatientSelected(e.target.value);
              }}
            >
              <option value="">Selecione um paciente</option>
              {pacients.map(pacient => (
                <option value={pacient.id} key={pacient.id}>
                  {pacient.first_name}
                </option>
              ))}
            </Select>

            <TextField
              label="Selecione a data"
              type="date"
              defaultValue={schedule.date}
              onChange={setScheduleDate}
              fullWidth
            />

            <TextField
              label="Selecione o horário"
              type="time"
              fullWidth
              defaultValue={schedule.hour}
              onChange={setScheduleHour}
            />
          </Agenda>
        </Modal>
      )}
    </Container>
  );
};

export default Dasboard;
