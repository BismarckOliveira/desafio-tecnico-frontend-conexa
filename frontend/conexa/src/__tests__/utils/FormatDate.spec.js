import { FormatDate } from '../../utils/FormatDate'

const date = 'Fri Feb 05 2021 10:20:00 GMT-0300 (Brasilia Standard Time)'


it('should be able to formated date', () => {
 expect(FormatDate({date})).toEqual('05/02/2021 10:20:00')
} )
